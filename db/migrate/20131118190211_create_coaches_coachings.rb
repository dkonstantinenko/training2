class CreateCoachesCoachings < ActiveRecord::Migration
  def change
    create_table :coaches_coachings do |t|
      t.integer :coach_id
      t.integer :coaching_id
      t.datetime :plece_date

      t.timestamps
    end
  end
end
