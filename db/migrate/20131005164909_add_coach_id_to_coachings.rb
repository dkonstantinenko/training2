class AddCoachIdToCoachings < ActiveRecord::Migration
  def change
    add_column :coachings, :coach_id, :integer
  end
end
