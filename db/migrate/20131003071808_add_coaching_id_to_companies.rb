class AddCoachingIdToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :coaching_id, :integer
  end
end
