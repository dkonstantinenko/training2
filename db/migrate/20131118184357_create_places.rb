class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.integer :town_id
      t.integer :coaching_id
      t.datetime :plece_date

      t.timestamps
    end
  end
end
