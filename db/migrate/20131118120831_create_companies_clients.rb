class CreateCompaniesClients < ActiveRecord::Migration
  def change
    create_table :companies_clients, id: false do |t|
      t.integer :company_id
      t.integer :client_id

      t.timestamps
    end
  end
end
