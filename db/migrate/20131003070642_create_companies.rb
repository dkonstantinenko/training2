class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :telephone
      t.string :email
      t.string :site
      t.string :address
      t.string :contact_person
      t.string :detail_information
      t.integer :coach_id
      t.string :client

      t.timestamps
    end
  end
end
