class RemoveCoachIdToCompanies < ActiveRecord::Migration
  def up
    remove_column :companies, :coach_id
  end

  def down
    add_column :companies, :coach_id, :integer
  end
end
