class RemoveClientToCompanies < ActiveRecord::Migration
  def up
    remove_column :companies, :client
  end

  def down
    add_column :companies, :client, :string
  end
end
