class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :name
      t.integer :company_id
      t.integer :coach_id
      t.string :detail_information
      t.string :comment

      t.timestamps
    end
  end
end
