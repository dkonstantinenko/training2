class CreateCommenttonews < ActiveRecord::Migration
  def change
    create_table :commenttonews do |t|
      t.string :comment
      t.integer :coach_id
      t.integer :news_id

      t.timestamps
    end
  end
end
