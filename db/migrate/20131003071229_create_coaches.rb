class CreateCoaches < ActiveRecord::Migration
  def change
    create_table :coaches do |t|
      t.string :name
      t.string :telephone
      t.string :email
      t.string :site
      t.integer :company_id
      t.string :detail_information
      t.integer :coaching_id
      t.string :comment
      t.string :article

      t.timestamps
    end
  end
end
