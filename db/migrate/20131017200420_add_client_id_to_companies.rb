class AddClientIdToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :client_id, :integer
  end
end
