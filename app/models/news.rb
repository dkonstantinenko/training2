class News < ActiveRecord::Base
  attr_accessible :coach_id, :comment, :company_id, :detail_information, :name, :commenttonews_id

  belongs_to :coach

  belongs_to :company

  has_many :commenttonews
end
