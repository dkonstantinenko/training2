class Commenttonews < ActiveRecord::Base
  attr_accessible :coach_id, :comment, :news_id

  belongs_to :coach

  belongs_to :news
end
