class Client < ActiveRecord::Base
  attr_accessible :name

  has_many :companies_clients
  has_many :clients, through: :companies_clients
end
