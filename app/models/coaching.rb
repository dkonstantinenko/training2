class Coaching < ActiveRecord::Base
  attr_accessible :category_id, :company_id, :date, :name, :online, :price, :town_id, :coach_id

  has_and_belongs_to_many :coaches

  belongs_to :company

  has_many :places
  has_many :towns, through: :places

  belongs_to :category

end
