class Town < ActiveRecord::Base
  attr_accessible :name

  has_many :places
  has_many :coachings, through: :places
  
end
