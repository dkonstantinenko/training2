class CompaniesClient < ActiveRecord::Base
  attr_accessible :client_id, :company_id

  belongs_to :client
  belongs_to :company
end
