class Place < ActiveRecord::Base
  attr_accessible :coaching_id, :plece_date, :town_id

  belongs_to :town
  belongs_to :coaching
end
