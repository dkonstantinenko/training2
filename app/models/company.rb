class Company < ActiveRecord::Base
  attr_accessible :address, :client_id, :coach_id, :contact_person, :detail_information, :email, :name, :site, :telephone, :news_id

  has_many :coachings

  has_many :coaches

  has_many :news

  has_many :companies_clients
  has_many :companies, through: :companies_clients

end
