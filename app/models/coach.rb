class Coach < ActiveRecord::Base
  attr_accessible :article, :coaching_id, :comment, :company_id, :detail_information, :email, :name, :site, :telephone, :news_id, :commenttonews_id

  has_many :news

  has_and_belongs_to_many :coachings

  belongs_to :company

  has_many :commenttonews

end
