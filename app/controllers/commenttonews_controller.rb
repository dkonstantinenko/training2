class CommenttonewsController < ApplicationController
  # GET /commenttonews
  # GET /commenttonews.json
  def index
    @commenttonews = Commenttonews.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @commenttonews }
    end
  end

  # GET /commenttonews/1
  # GET /commenttonews/1.json
  def show
    @commenttonews = Commenttonews.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @commenttonews }
    end
  end

  # GET /commenttonews/new
  # GET /commenttonews/new.json
  def new
    @commenttonews = Commenttonews.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @commenttonews }
    end
  end

  # GET /commenttonews/1/edit
  def edit
    @commenttonews = Commenttonews.find(params[:id])
  end

  # POST /commenttonews
  # POST /commenttonews.json
  def create
    @commenttonews = Commenttonews.new(params[:commenttonews])

    respond_to do |format|
      if @commenttonews.save
        format.html { redirect_to @commenttonews, notice: 'Commenttonews was successfully created.' }
        format.json { render json: @commenttonews, status: :created, location: @commenttonews }
      else
        format.html { render action: "new" }
        format.json { render json: @commenttonews.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /commenttonews/1
  # PUT /commenttonews/1.json
  def update
    @commenttonews = Commenttonews.find(params[:id])

    respond_to do |format|
      if @commenttonews.update_attributes(params[:commenttonews])
        format.html { redirect_to @commenttonews, notice: 'Commenttonews was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @commenttonews.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /commenttonews/1
  # DELETE /commenttonews/1.json
  def destroy
    @commenttonews = Commenttonews.find(params[:id])
    @commenttonews.destroy

    respond_to do |format|
      format.html { redirect_to commenttonews_index_url }
      format.json { head :no_content }
    end
  end
end
