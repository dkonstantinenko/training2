class CompaniesClientsController < ApplicationController
  # GET /companies_clients
  # GET /companies_clients.json
  def index
    @companies_clients = CompaniesClient.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @companies_clients }
    end
  end

  # GET /companies_clients/1
  # GET /companies_clients/1.json
  def show
    @companies_client = CompaniesClient.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @companies_client }
    end
  end

  # GET /companies_clients/new
  # GET /companies_clients/new.json
  def new
    @companies_client = CompaniesClient.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @companies_client }
    end
  end

  # GET /companies_clients/1/edit
  def edit
    @companies_client = CompaniesClient.find(params[:id])
  end

  # POST /companies_clients
  # POST /companies_clients.json
  def create
    @companies_client = CompaniesClient.new(params[:companies_client])

    respond_to do |format|
      if @companies_client.save
        format.html { redirect_to @companies_client, notice: 'Companies client was successfully created.' }
        format.json { render json: @companies_client, status: :created, location: @companies_client }
      else
        format.html { render action: "new" }
        format.json { render json: @companies_client.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /companies_clients/1
  # PUT /companies_clients/1.json
  def update
    @companies_client = CompaniesClient.find(params[:id])

    respond_to do |format|
      if @companies_client.update_attributes(params[:companies_client])
        format.html { redirect_to @companies_client, notice: 'Companies client was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @companies_client.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /companies_clients/1
  # DELETE /companies_clients/1.json
  def destroy
    @companies_client = CompaniesClient.find(params[:id])
    @companies_client.destroy

    respond_to do |format|
      format.html { redirect_to companies_clients_url }
      format.json { head :no_content }
    end
  end
end
