class CoachesCoachingsController < ApplicationController
  # GET /coaches_coachings
  # GET /coaches_coachings.json
  def index
    @coaches_coachings = CoachesCoaching.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @coaches_coachings }
    end
  end

  # GET /coaches_coachings/1
  # GET /coaches_coachings/1.json
  def show
    @coaches_coaching = CoachesCoaching.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @coaches_coaching }
    end
  end

  # GET /coaches_coachings/new
  # GET /coaches_coachings/new.json
  def new
    @coaches_coaching = CoachesCoaching.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @coaches_coaching }
    end
  end

  # GET /coaches_coachings/1/edit
  def edit
    @coaches_coaching = CoachesCoaching.find(params[:id])
  end

  # POST /coaches_coachings
  # POST /coaches_coachings.json
  def create
    @coaches_coaching = CoachesCoaching.new(params[:coaches_coaching])

    respond_to do |format|
      if @coaches_coaching.save
        format.html { redirect_to @coaches_coaching, notice: 'Coaches coaching was successfully created.' }
        format.json { render json: @coaches_coaching, status: :created, location: @coaches_coaching }
      else
        format.html { render action: "new" }
        format.json { render json: @coaches_coaching.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /coaches_coachings/1
  # PUT /coaches_coachings/1.json
  def update
    @coaches_coaching = CoachesCoaching.find(params[:id])

    respond_to do |format|
      if @coaches_coaching.update_attributes(params[:coaches_coaching])
        format.html { redirect_to @coaches_coaching, notice: 'Coaches coaching was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @coaches_coaching.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /coaches_coachings/1
  # DELETE /coaches_coachings/1.json
  def destroy
    @coaches_coaching = CoachesCoaching.find(params[:id])
    @coaches_coaching.destroy

    respond_to do |format|
      format.html { redirect_to coaches_coachings_url }
      format.json { head :no_content }
    end
  end
end
